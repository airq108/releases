
# Sensus Firmware Flashing Guide for Windows

## Prerequisites
1. **Python Installed**: Ensure Python is installed on the computer.
2. **esptool.py**: This tool is required for flashing Sensus devices.
3. **CP210x USB to UART Bridge VCP Drivers**: Required for communication with the Sensus device.

## Step-by-Step Guide

### 1. Download and Install Python
- Go to [python.org](https://www.python.org/downloads/).
- Download and install Python. During installation, make sure to check the box that says "Add Python to PATH".

### 1.1 ALTERNATIVE TOOL
- [https://web.esphome.io/](https://web.esphome.io/)

### 2. Install esptool.py
- Open Command Prompt (press `Win + R`, type `cmd`, and press Enter).
- Run the following command to install esptool:
  ```
  pip install esptool
  ```

### 3. Download and Install CP210x USB to UART Bridge VCP Drivers
- Go to the [Silicon Labs website](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers) to download the CP210x USB to UART Bridge VCP drivers. (https://www.silabs.com/documents/public/software/CP210x_Universal_Windows_Driver.zip). ALTERNATIVELLY, YOU CAN USE THE DRIVERS FOLDER IN THIS REPO
- Select the appropriate driver for your operating system (Windows).
- Once downloaded, open the installer and follow the on-screen instructions to install the driver.
- After the installation is complete, restart your computer if prompted.

### 4. Connect the Sensus Device
- Connect the Sensus device to the computer using a USB cable.

### 5. Verify the COM Port
- Open Device Manager (press `Win + X` and select `Device Manager`).
- Expand the "Ports (COM & LPT)" section. You should see "Silicon Labs CP210x USB to UART Bridge (COMX)" where `COMX` is the port number assigned to the device.

### 6. Prepare the Firmware
- Place the binary file (let's call it `firmware.bin`) in an accessible directory on your computer, such as `C:\firmware\`.

### 7. Flash the Firmware
- Open Command Prompt.
- Navigate to the directory containing `firmware.bin` by typing:
  ```
  cd C:\firmware
  ```
- Run the following command to flash the firmware:
  ```
  esptool.py --chip esp32 --port COMX --baud 115200 write_flash -z 0x1000 firmware.bin
  ```
  Replace `COMX` with the actual COM port identified in the previous step.

### Example Command
  ```
  esptool.py --chip esp32 --port COM3 --baud 115200 write_flash -z 0x1000 firmware.bin
  ```

### 8. Verify the Flashing Process
- The esptool will output progress and any errors. Ensure that the flashing process completes successfully without errors.

```
screen COM3 115200
```

## Additional Tips
- Ensure the Sensus device is in bootloader mode if necessary. This might require holding down the `BOOT` button while connecting the device or resetting it during the process.
- Double-check the binary file is compatible with the Sensus hardware version.

By following these steps, you should be able to flash the firmware to the Sensus device using a Windows computer without needing to install the Arduino IDE.

- to find if the open is open
```
lsof COM3
```
- to delete the screen
```
kill -9 <pid>
```
- to reset the port
```
stty sane < COM3
```


# Sensus Firmware Flashing Guide for macOS

## Prerequisites
1. **Python Installed**: Ensure Python is installed on the computer.
2. **esptool.py**: This tool is required for flashing Sensus devices.
3. **CP210x USB to UART Bridge VCP Drivers**: Required for communication with the Sensus device.

## Step-by-Step Guide

### 1. Download and Install Python
- Go to [python.org](https://www.python.org/downloads/).
- Download and install Python. During installation, make sure to add Python to your PATH.

### 2. Install esptool.py
- Open Terminal.
- Run the following command to install esptool:
  ```sh
  pip install esptool
  ```

### 3. Download and Install CP210x USB to UART Bridge VCP Drivers
- Go to the [Silicon Labs website](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers) to download the CP210x USB to UART Bridge VCP drivers.
- Select the appropriate driver for macOS.
- Once downloaded, open the installer and follow the on-screen instructions to install the driver.
- After the installation is complete, restart your computer if prompted.

### 4. Connect the Sensus Device
- Connect the Sensus device to the computer using a USB cable.

### 5. Verify the COM Port
- Open Terminal and run the following command to list the available serial ports:
  ```sh
  ls /dev/tty.*
  ```
- Identify the port corresponding to your device. It will look something like `/dev/tty.SLAB_USBtoUART`.

### 6. Prepare the Firmware
- Place the binary file (let's call it `firmware.bin`) in an accessible directory on your computer, such as `~/firmware/`.

### 7. Flash the Firmware
- Open Terminal.
- Navigate to the directory containing `firmware.bin` by typing:
  ```sh
  cd ~/firmware
  ```
- Run the following command to flash the firmware:
  ```sh
  esptool.py --chip esp32 --port /dev/tty.SLAB_USBtoUART --baud 115200 write_flash -z 0x1000 firmware.bin
  ```

### 8. Verify the Flashing Process
- The esptool will output progress and any errors. Ensure that the flashing process completes successfully without errors.

## Additional Tips
- Ensure the Sensus device is in bootloader mode if necessary. This might require holding down the `BOOT` button while connecting the device or resetting it during the process.
- Double-check the binary file is compatible with the Sensus hardware version.

By following these steps, you should be able to flash the firmware to the Sensus device using a macOS computer without needing to install the Arduino IDE.

# Sensus Firmware Flashing Hack:

### 1. Bypassing hurdle and uploading file.

- MERGE FILES
```
esptool.py --chip ESP32 merge_bin -o merged-flash.bin --flash_mode dio --flash_freq 80m --flash_size 16MB 0x1000 bootloader_dio_80m.bin 0x8000 latest.partitions.bin 0x10000 latest.bin
```

- WRITE TO FLASH
```
esptool.py --chip esp32 --port /dev/cu.SLAB_USBtoUART --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0x0 merged-flash.bin
```